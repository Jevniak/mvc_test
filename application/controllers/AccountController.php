<?php

namespace application\controllers;

use application\core\Controller;

class AccountController extends Controller{
    
    public $db;

    public function loginAction() {
        if (!empty($_POST)) {
            $param = [
                'username' => $_POST['username'],
                'password' => md5($_POST['password']),
            ];
            $result = $this->model->login($param);

            if ($result) {
                $_SESSION['auth'] = true;
                $this->view->location('/');
                $this->view->message($result,'Неверный логин или пароль');
            } else {
                $this->view->message('Ошибка входа','Неверный логин или пароль');
            }

        }
        $this->view->render('Авторизация');
    }

    public function logoutAction() {
        unset($_SESSION['auth']);
        session_destroy();
        $this->view->redirect('/');
        // $this->view->render('регистрация');
    }

}