<?php

namespace application\controllers;

use application\core\Controller;


class MainController extends Controller{
    public function indexAction() {
        if (!empty($_POST)) {
            $action = $_POST['action'];
            if ($action == 'performed') {
                $result = $this->model->completeTask($_POST['id']);      
                exit(json_encode($result));
            } else if ($action == 'edit') {
                $result = $this->model->editTask($_POST['id'],$_POST['new_text']);    
                exit(json_encode($result));
            } else if ($action == 'add') {
                $username = $_POST['name'];
                $email = $_POST['email'];
                $desc = strip_tags($_POST['desc_text']);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->view->message('ошибка','не корректный email');
                }
                if (strlen($username) == 0 || strlen($email) == 0 || strlen($desc) == 0) {
                    $this->view->message('ошибка','заполните пустые поля');
                }
                $param = [
                    'username' => $username,
                    'email' => $email,
                    'desc_text' => $desc,
                ];
                $result = $this->model->addTask($param);
                $this->view->message('Успех','Задача успешно создана');

            }
        }
        $page = 1;
        $sorted = false;
        if (!empty($_GET)) {
            $page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
            $col = (!empty($_GET['col'])) ? $_GET['col'] : '' ;
            $sort = (!empty($_GET['sort'])) ? $_GET['sort'] : '' ;
            if (!empty($col) && !empty($sort)) {
                $sorted = true;
                $result = $this->model->getTasksCountPages($page,$col,$sort);
            }
        }

        if (!$sorted)
            $result = $this->model->getTasksCountPages($page);
        $vars = [
            'tasks' => $result,
        ];       
        $this->view->render('Главная страница',$vars);
    }
}