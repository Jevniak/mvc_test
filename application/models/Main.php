<?php

namespace application\models;

use application\core\Model;

class Main extends Model {

    protected $limitPages = 3;
    
    public function getTasks($start, $end) {                
        $result = $this->db->row('SELECT * FROM tasks LIMIT '.$start.', '.$end);
        return $result;
    }

    public function getTasksOrder($start, $end, $col, $sort) {                
        $result = $this->db->row('SELECT * FROM tasks ORDER BY '.$col.' '.$sort.' LIMIT '.$start.', '.$end);
        return $result;
    }

    public function getTasksCountPages($page, $col = null, $sort = null) {
        $limit = $this->limitPages;
        $start = $page * $limit - $limit;
        $end = $limit;
        $pages = $this->db->column('SELECT COUNT(*) FROM tasks');
        if (!empty($col) && !empty($sort)) {
            $result['items'] = $this->getTasksOrder($start,$end,$col,$sort);
        } else {
            $result['items'] = $this->getTasks($start,$end);
        }
        
        $result['count_page'] = ceil($pages/3);
        return $result;
    }

    public function completeTask($id) {
        $param = [
            'id' => $id,
        ];
        $this->db->column('UPDATE tasks SET performed=true WHERE id=:id',$param);
        return [
            'action' => 'performed',
            'id' => $id,
        ];        
    }

    public function editTask($id, $description) {
        $param = [
            'desc_text' => $description,
            'id' => $id,
        ];
        $result = $this->db->column('UPDATE tasks SET edit=true, desc_text=:desc_text WHERE id=:id',$param);
        return [
            'action' => 'edit',
            'id' => $id,
            'text' => $description,
        ];        
    }

    public function addTask($param) {
        $result = $this->db->column('INSERT INTO tasks (username,email,desc_text) VALUES (:username,:email,:desc_text)',$param);
        $param['action'] = 'add';
        return $param;
    }

}