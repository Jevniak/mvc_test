<?php if (!isset($_SESSION['auth'])):?>
<div class="container row d-flex justify-content-center flex-column align-items-center">

<h3>Авторизация</h3>
<form action="/account/login" method="POST">
    <p>Логин</p>
    <input type="text" name="username">
    <p>Пароль</p>
    <input type="password" name="password">
    <br>
    <button type="submit" class="btn btn-success mr-5 mt-2">Вход</button>
    <a href="/">На главную</a>
</form>
</div>
<?php else: ?>
<h3>Вы уже авторизованы</h3>
<a href="/">На главную</a>
<?php endif; ?>