<?php if (!isset($_SESSION['auth'])) : ?>
  <a href="account/login">Авторизация</a>
<?php else : ?>
  <a href="account/logout">Выйти</a>
<?php endif; ?>
<div class="row col-sm container d-flex flex-column justify-content-center">
  <div class="container pb-5">

    <form action="" method="POST" >
      <input type="hidden" name="action" value="add">

      <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" name="name" id="name" class="form-control">

      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control">
      </div>
      <div class="form-group">
        <label for="desc">Описание задачи</label>
        <textarea name="desc_text" id="desc" rows="3" class="form-control"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Создать</button>
    </form>
  </div>

  <?php
  $page = (!empty($_GET['page'])) ? '&page=' . $_GET['page'] : '';
  ?>
  <div class="d-flex justify-content-around container pb-5">
    <div>
      <p>Сортировка по имени:</p>
      <a href="?col=username&sort=desc<?= $page ?>">по убыванию |</a>
      <a href="?col=username&sort=asc<?= $page ?>">по возрастанию</a>
    </div>
    <div>
      <p>Сортировка по email:</p>
      <a href="?col=email&sort=desc<?= $page ?>">по убыванию |</a>
      <a href="?col=email&sort=asc<?= $page ?>">по возрастанию</a>
    </div>
    <div>
      <p>Сортировка по описанию:</p>
      <a href="?col=desc_text&sort=desc<?= $page ?>">по убыванию |</a>
      <a href="?col=desc_text&sort=asc<?= $page ?>">по возрастанию</a>
    </div>
  </div>
  <?php foreach ($vars['tasks']['items'] as $val) : ?>
    <div class="card container-fluid mr-3" data-id=<?= $val['id'] ?>>
      <div class="card-header">
        <p class="card-username"> <?= $val['username'] ?></p>
        <p class="card-email"> <a href="mailto:<?= $val['email'] ?>"> <?= $val['email'] ?></a></p>
        <?php if ($val['edit']) : ?>
          <p class="changed">Отредактировано администратором</p>
        <?php endif; ?>
      </div>
      <div class="card-body">
        <p class="card-text"><?= $val['desc_text'] ?></p>

        <?php if (isset($_SESSION['auth']) && !$val['performed']) : ?>
          <form action="" method="POST">
            <input type="hidden" name="id" value="<?= $val['id'] ?>">
            <input type="hidden" name="action" value="performed">
            <button type="submit" class="btn btn-success">Завершить задачу</button>
          </form>
          <br>
          <form action="" method="POST">
            <input type="hidden" name="id" value="<?= $val['id'] ?>">
            <input type="hidden" name="action" value="edit">
            <textarea name="new_text" id="new_text" rows="10" style="display: none;" class="form-control"></textarea>
            <button type="submit" class="btn btn-primary" id="change">Редактировать</button>
          </form>
        <?php elseif ($val['performed']) : ?>
          <p>Задача выполнена</p>
        <?php else : ?>
          <p>Задача в процессе</p>
        <?php endif; ?>

      </div>
    </div>
    <br>

  <?php endforeach; ?>
  <nav aria-label="">

    <ul class="pagination justify-content-center ">
      <?php
      if ($vars['tasks']['count_page'] > 1) :
        for ($i = 1; $i <= $vars['tasks']['count_page']; $i++) :
      ?>
          <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
      <?php
        endfor;
      endif;
      ?>

    </ul>
  </nav>

</div>