$(document).ready(function() {
	$('form').submit(function(event) {
		var json;
		event.preventDefault();
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData: false,
			success: function(result) {
				console.log(result);
				json = jQuery.parseJSON(result);
				if (json.url) {
					window.location.href = json.url;
				} else {
					console.log(json);
					let el = $('.card[data-id='+json.id+']');
					if (json.action == 'performed') {
						
						el.find('form').remove();
						el.find('.card-body').append('<p>Задача выполнена</p>');
						return;
					} else if (json.action == "edit") {
						el.find('.card-text').text(json.text);
						if (el.find('.changed').length == 0) {
							el.find('.card-header').append('<p class="changed">Изменено</p>');
						}
						return;
					} else if (json.action == "add") {
						return;
					}
					alert(json.status + ' - ' + json.message);
					if (json.status == 'Успех') {
						$(location).attr('href', '/')
					}
				}
			},
		});
	});

	let tasks = $('.card');

	tasks.each(function() {
		$(this).find('#change').on('click', (e) => {
			console.log($(this).find('#change').is(':visible'));
			if ($(this).find('textarea').is(':hidden')) {
				e.preventDefault();
				$(this).find('textarea').val($(this).find('.card-text').text());
				$(this).find('textarea').slideDown();
			} else {
				if ($(this).find('textarea').val() == $(this).find('.card-text').text()) {
					e.preventDefault();
				}
				$(this).find('textarea').slideUp();
			}
		})
	});

	
});